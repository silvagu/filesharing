package com.filesharing.fs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.filesharing.fs.FsApplication;
import com.filesharing.fs.model.User;
import com.filesharing.fs.model.dto.UserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = FsApplication.class)
@AutoConfigureTestEntityManager
@Transactional
@AutoConfigureMockMvc
class MainRestControllerTest {
  private static final ObjectMapper mapper = new ObjectMapper();

  @Autowired private MockMvc mockMvc;
  @Autowired private TestEntityManager entityManager;

  @Test
  void register_Should_Create_An_User_And_Return_Code201() throws Exception {
    UserDTO userDTO = new UserDTO("steve", "1234");

    mockMvc
        .perform(
            post("/register")
                .content(mapper.writeValueAsString(userDTO))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated());

    User savedUser = entityManager.find(User.class, 4L);

    assertNotNull(savedUser);
    assertEquals(savedUser.getEmail(), userDTO.getEmail());
    assertEquals(savedUser.getPassword(), userDTO.getPassword());
  }

  @Test
  void register_Should_Not_Create_An_User_And_Return_Code400() throws Exception {
    mockMvc
        .perform(post("/register").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());

    User savedUser = entityManager.find(User.class, 4L);
    assertNull(savedUser);
  }

  @Test
  void register_Should_Allow_Only_Post() throws Exception {
    mockMvc
        .perform(get("/register").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isMethodNotAllowed());
    mockMvc
        .perform(put("/register").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isMethodNotAllowed());
    mockMvc
        .perform(delete("/register").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isMethodNotAllowed());
  }
}
