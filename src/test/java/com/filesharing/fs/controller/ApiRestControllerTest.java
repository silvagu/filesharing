package com.filesharing.fs.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filesharing.fs.model.User;
import com.filesharing.fs.model.dto.OwnedSharedDTO;
import com.filesharing.fs.model.dto.ShareDTO;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    properties = {"upload.directory=src/test/resource"})
@AutoConfigureTestEntityManager
@Transactional
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApiRestControllerTest {
  private static final ObjectMapper mapper = new ObjectMapper();

  @Autowired private MockMvc mockMvc;
  @Autowired private TestEntityManager entityManager;
  @Autowired private TestRestTemplate testRestTemplate;

  @Test
  @Order(1)
  void unauthorized_requests() throws Exception {
    mockMvc.perform(get("/api/file")).andExpect(status().isUnauthorized());
    mockMvc.perform(get("/api/file/1")).andExpect(status().isUnauthorized());
    mockMvc.perform(post("/api/file")).andExpect(status().isUnauthorized());
    mockMvc.perform(get("/api/share")).andExpect(status().isUnauthorized());
  }

  @Test
  @Order(2)
  void getFiles_Should_Return_List_Of_Files_Owned_An_Shared_With_An_Authenticated_User()
      throws Exception {
    ResponseEntity<OwnedSharedDTO> response =
        testRestTemplate
            .withBasicAuth("bob@martin.com", "uncle")
            .getForEntity("/api/file", OwnedSharedDTO.class);

    assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(response.getBody().getOwned().size(), 2);
    assertEquals(response.getBody().getShared().size(), 1);
  }

  @Test
  @Order(3)
  void shareFile_Should_Return_Bad_Request_For_An_Email_That_Doesnt_exist()
      throws JsonProcessingException {
    ShareDTO shareDTO = new ShareDTO("some@email.com", "5");
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(shareDTO), headers);
    ResponseEntity<String> responseEntity =
        testRestTemplate
            .withBasicAuth("admin@me.com", "admin")
            .exchange("/api/share", HttpMethod.POST, entity, String.class);

    assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
  }

  @Test
  @Order(4)
  void shareFile_Should_Share_File_With_Another_User() throws JsonProcessingException {
    ShareDTO shareDTO = new ShareDTO("bob@martin.com", "5");
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(shareDTO), headers);
    ResponseEntity<String> responseEntity =
        testRestTemplate
            .withBasicAuth("admin@me.com", "admin")
            .exchange("/api/share", HttpMethod.POST, entity, String.class);

    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

    User bob = entityManager.find(User.class, 3L);
    assertEquals(bob.getFiles().size(), 2);
  }

  @Test
  @Order(5)
  void getFile_Should_Download_A_File_If_A_Authenticated_User_Owns_The_File() {
    ResponseEntity<Resource> responseEntity =
        testRestTemplate
            .withBasicAuth("admin@me.com", "admin")
            .getForEntity("/api/file/1", Resource.class);

    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    assertEquals("attachment", responseEntity.getHeaders().getContentDisposition().getType());
    assertEquals("file1.txt", responseEntity.getHeaders().getContentDisposition().getFilename());
  }

  @Test
  @Order(6)
  void getFile_Should_Download_A_File_If_The_File_Is_Shared_With_The_Authenticated_User() {
    ResponseEntity<Resource> responseEntity =
        testRestTemplate
            .withBasicAuth("admin@me.com", "admin")
            .getForEntity("/api/file/1", Resource.class);

    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    assertEquals("attachment", responseEntity.getHeaders().getContentDisposition().getType());
    assertEquals("file1.txt", responseEntity.getHeaders().getContentDisposition().getFilename());
  }

  @Test
  @Order(7)
  void
      getFile_Should_Not_Download_A_File_If_A_Authenticated_User_Does_Not_Own_The_File_Or_It_Is_Not_Shared_With_Him() {
    ResponseEntity<Resource> responseEntity =
        testRestTemplate
            .withBasicAuth("bob@martin.com", "uncle")
            .getForEntity("/api/file/2", Resource.class);

    assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    assertNull(responseEntity.getHeaders().getContentDisposition().getFilename());
  }

  @Test
  void uploadFile_Should_Upload_A_File_Sent_By_A_Authenticated_User() throws Exception {
    MockMultipartFile multipartFile =
        new MockMultipartFile("file", "text.txt", "text/plain", "test upload".getBytes());

    MvcResult mvcResult =
        mockMvc
            .perform(
                multipart("/api/file")
                    .file(multipartFile)
                    .with(
                        SecurityMockMvcRequestPostProcessors.httpBasic("bob@martin.com", "uncle")))
            .andExpect(status().isOk())
            .andReturn();

    assertEquals("6", mvcResult.getResponse().getContentAsString());
  }
}
