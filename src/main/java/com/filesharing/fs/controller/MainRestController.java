package com.filesharing.fs.controller;

import com.filesharing.fs.model.dto.UserDTO;
import com.filesharing.fs.service.UserService;
import com.filesharing.fs.service.dto.UserDtoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class MainRestController {
  private final UserService userService;
  private final UserDtoService userDtoService;

  public MainRestController(UserService userService, UserDtoService userDtoService) {
    this.userService = userService;
    this.userDtoService = userDtoService;
  }

  @PostMapping("/register")
  public ResponseEntity register(@RequestBody UserDTO userDTO) {
    userService.createUser(userDtoService.toEntity(userDTO));
    return new ResponseEntity(HttpStatus.CREATED);
  }
}
