package com.filesharing.fs.controller;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;
import com.filesharing.fs.model.dto.OwnedSharedDTO;
import com.filesharing.fs.model.dto.ShareDTO;
import com.filesharing.fs.service.FileService;
import com.filesharing.fs.service.StorageService;
import com.filesharing.fs.service.UserService;
import com.filesharing.fs.service.dto.FileDtoService;
import com.filesharing.fs.service.dto.UserDtoService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(value = "/api")
public class ApiRestController {

  private final UserService userService;
  private final FileService fileService;
  private final FileDtoService fileDtoService;
  private final StorageService storageService;

  public ApiRestController(
      FileService fileService,
      UserService userService,
      FileDtoService fileDtoService,
      StorageService storageService) {
    this.fileService = fileService;
    this.userService = userService;
    this.fileDtoService = fileDtoService;
    this.storageService = storageService;
  }

  @GetMapping("/file")
  public ResponseEntity<OwnedSharedDTO> getFiles(Principal principal) {
    Optional<User> authenticatedUser = userService.getUserByEmail(principal.getName());
    if (authenticatedUser.isPresent()) {
      Set<File> filesOwned = fileService.getFilesOwnedByUserId(authenticatedUser.get().getId());
      Set<File> filesShared = authenticatedUser.get().getFiles();

      return new ResponseEntity<>(fileDtoService.toDto(filesOwned, filesShared), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @GetMapping("/file/{id}")
  public ResponseEntity getFile(@PathVariable("id") String id, Principal principal) {
    Optional<User> user = userService.getUserByEmail(principal.getName());

    if (user.isPresent()) {
      Optional<Resource> resource = fileService.getUserFile(user.get(), Long.parseLong(id));
      if (resource.isPresent()) {
        String contentDispositionValue =
            "attachment; filename=\"" + resource.get().getFilename() + "\"";
        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, contentDispositionValue)
            .body(resource.get());
      }
    }

    return new ResponseEntity(HttpStatus.FORBIDDEN);
  }

  @PostMapping("/file")
  public ResponseEntity uploadFile(@RequestParam MultipartFile file, Principal principal) {
    try {
      Optional<User> user = userService.getUserByEmail(principal.getName());
      if (user.isPresent()) {
        String filename = storageService.store(file);
        File newFile = new File();
        newFile.setUser(user.get());
        newFile.setFileName(filename);
        fileService.saveFile(newFile);
        return new ResponseEntity(newFile.getId().toString(), HttpStatus.OK);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ResponseEntity(HttpStatus.BAD_REQUEST);
  }

  @PostMapping("/share")
  public ResponseEntity shareFile(@RequestBody ShareDTO shareDTO, Principal principal) {
    Optional<File> file = fileService.getFileById(Long.parseLong(shareDTO.getFileId()));
    if (file.isPresent()) {
      boolean result =
          userService.shareFileWithUser(principal.getName(), shareDTO.getEmail(), file.get());
      if (result) {
        return new ResponseEntity(HttpStatus.OK);
      }
    }
    return new ResponseEntity(HttpStatus.BAD_REQUEST);
  }
}
