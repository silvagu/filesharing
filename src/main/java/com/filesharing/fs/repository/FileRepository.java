package com.filesharing.fs.repository;

import com.filesharing.fs.model.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
  Set<File> findAllByUserId(Long userId);
}
