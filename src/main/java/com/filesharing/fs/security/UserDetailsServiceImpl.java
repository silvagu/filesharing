package com.filesharing.fs.security;

import com.filesharing.fs.model.User;
import com.filesharing.fs.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

  private final UserService userServiceImpl;

  public UserDetailsServiceImpl(UserService userServiceImpl) {
    this.userServiceImpl = userServiceImpl;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Optional<User> user = userServiceImpl.getUserByEmail(email);

    if (user.isPresent()) {
      org.springframework.security.core.userdetails.User.UserBuilder builder =
          org.springframework.security.core.userdetails.User.withUsername(email);
      builder.password(user.get().getPassword());
      builder.authorities("REGISTERED");
      logger.info("Request login from: " + user.get().getEmail());
      return builder.build();
    }

    throw new UsernameNotFoundException("Invalid Login/Password.");
  }
}
