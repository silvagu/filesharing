package com.filesharing.fs.config;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;
import com.filesharing.fs.repository.FileRepository;
import com.filesharing.fs.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataDBInitializer {
  private static final Logger logger = LoggerFactory.getLogger(DataDBInitializer.class);
  private List<User> userList = new ArrayList<>();

  @Autowired private UserService userService;

  @Autowired private FileRepository fileRepository;

  @AllArgsConstructor
  private enum UserData {
    ADMIN("admin@me.com", "admin"),
    JOHN("john@doe.com", "1234"),
    BOB("bob@martin.com", "uncle");

    private final String email;
    private final String password;
  }

  private void init() {
    logger.info("Data initialize");
    dataInit();
    logger.info("Initialization Completed!");
  }

  private void dataInit() {
    createUsers();
    createFiles();
  }

  private void createUsers() {
    User admin = new User();
    admin.setEmail(UserData.ADMIN.email);
    admin.setPassword(UserData.ADMIN.password);

    userService.createUser(admin);

    User john = new User();
    john.setEmail(UserData.JOHN.email);
    john.setPassword(UserData.JOHN.password);

    userService.createUser(john);

    User bob = new User();
    bob.setEmail(UserData.BOB.email);
    bob.setPassword(UserData.BOB.password);

    userService.createUser(bob);

    this.userList.addAll(Arrays.asList(admin, john, bob));
  }

  private void createFiles() {
    File f1 = new File();
    File f2 = new File();
    File f3 = new File();
    File f4 = new File();
    File f5 = new File();

    f1.setFileName("file1.txt");
    f2.setFileName("file2.txt");
    f3.setFileName("file3.txt");
    f4.setFileName("file4.txt");
    f5.setFileName("file5.txt");

    f1.setUser(userList.get(0));
    f2.setUser(userList.get(0));
    f3.setUser(userList.get(2));
    f4.setUser(userList.get(2));
    f5.setUser(userList.get(0));

    this.fileRepository.saveAll(Arrays.asList(f1, f2, f3, f4, f5));

    this.userList.get(1).addFile(f1);
    this.userList.get(1).addFile(f2);
    this.userList.get(2).addFile(f1);

    this.userService.createUser(this.userList.get(1));
    this.userService.createUser(this.userList.get(2));
  }
}
