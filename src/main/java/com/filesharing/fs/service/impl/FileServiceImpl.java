package com.filesharing.fs.service.impl;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;
import com.filesharing.fs.repository.FileRepository;
import com.filesharing.fs.service.FileService;
import com.filesharing.fs.service.StorageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.core.io.Resource;
import java.util.*;

@Service
@Transactional
public class FileServiceImpl implements FileService {
  private final FileRepository fileRepository;
  private final StorageService storageService;

  public FileServiceImpl(FileRepository fileRepository, StorageService storageService) {
    this.fileRepository = fileRepository;
    this.storageService = storageService;
  }

  @Override
  public Set<File> getFilesOwnedByUserId(Long userId) {
    return fileRepository.findAllByUserId(userId);
  }

  @Override
  public Optional<File> getFileById(Long id) {
    return fileRepository.findById(id);
  }

  @Override
  public void saveFile(File file) {
    fileRepository.save(file);
  }

  @Override
  public Optional<Resource> getUserFile(User user, Long fileId) {
    Optional<File> file = fileRepository.findById(fileId);
    if (file.isPresent()) {
      if (file.get().getUser() == user || user.getFiles().contains(file.get())) {
        Resource resource = storageService.loadAsResource(file.get().getFileName());
        return Optional.ofNullable(resource);
      }
    }
    return Optional.empty();
  }
}
