package com.filesharing.fs.service.impl;

import com.filesharing.fs.model.User;
import com.filesharing.fs.model.dto.UserDTO;
import com.filesharing.fs.service.dto.UserDtoService;
import org.springframework.stereotype.Service;

@Service
public class UserDtoServiceImpl implements UserDtoService {

  @Override
  public UserDTO toDto(User user) {
    if (user == null) {
      return null;
    }

    return new UserDTO(user.getEmail(), user.getPassword());
  }

  @Override
  public User toEntity(UserDTO userDTO) {
    if (userDTO == null) {
      return null;
    }

    User user = new User();
    user.setEmail(userDTO.getEmail());
    user.setPassword(userDTO.getPassword());

    return user;
  }
}
