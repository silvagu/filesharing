package com.filesharing.fs.service.impl;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;
import com.filesharing.fs.repository.UserRepository;
import com.filesharing.fs.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public void createUser(User user) {
    this.userRepository.save(user);
  }

  @Override
  public Optional<User> getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public boolean shareFileWithUser(String ownerEmail, String shareEmail, File file) {
    Optional<User> owner = userRepository.findByEmail(ownerEmail);
    if (owner.isPresent() && file.getUser() == owner.get()) {
      Optional<User> sharedUser = userRepository.findByEmail(shareEmail);
      if (sharedUser.isPresent()) {
        sharedUser.get().addFile(file);
        userRepository.save(sharedUser.get());
        return true;
      }
    }
    return false;
  }
}
