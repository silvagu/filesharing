package com.filesharing.fs.service.impl;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.dto.FileDTO;
import com.filesharing.fs.model.dto.OwnedSharedDTO;
import com.filesharing.fs.service.dto.FileDtoService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FileDtoServiceImpl implements FileDtoService {
  @Override
  public FileDTO toDto(File file) {
    if (file == null) {
      return null;
    }

    return new FileDTO(file);
  }

  @Override
  public File toEntity(FileDTO fileDTO) {
    return null;
  }

  public OwnedSharedDTO toDto(Set<File> filesOwned, Set<File> filesShared) {
    if (filesOwned == null || filesShared == null) {
      return null;
    }

    OwnedSharedDTO ownedSharedDTO = new OwnedSharedDTO();

    ownedSharedDTO.setOwned(toDto(filesOwned));
    ownedSharedDTO.setShared(toDto(filesShared));

    return ownedSharedDTO;
  }
}
