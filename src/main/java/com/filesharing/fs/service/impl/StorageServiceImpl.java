package com.filesharing.fs.service.impl;

import com.filesharing.fs.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Transactional
public class StorageServiceImpl implements StorageService {
  private static final Logger logger = LoggerFactory.getLogger(StorageServiceImpl.class);

  @Value("${upload.directory}")
  String uploadPath;

  @Override
  public String store(MultipartFile file) throws IOException {
    Path uploadDir = Paths.get(uploadPath);
    if (!Files.exists(uploadDir)) {
      Files.createDirectory(uploadDir);
    }
    String fileName = file.getOriginalFilename();
    file.transferTo(Paths.get(uploadPath + "/" + fileName));
    logger.info("File successfully uploaded");
    return fileName;
  }

  @Override
  public Resource loadAsResource(String filename) {
    try {
      Path file = load(filename);
      Resource resource = new UrlResource(file.toUri());
      if (resource.exists() || resource.isReadable()) {
        return resource;
      }
    } catch (MalformedURLException e) {
      e.printStackTrace();
      logger.error(e.getMessage());
    }
    return null;
  }

  private Path load(String filename) {
    return Paths.get(uploadPath).resolve(filename);
  }
}
