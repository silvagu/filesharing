package com.filesharing.fs.service.dto;

import com.filesharing.fs.model.User;
import com.filesharing.fs.model.dto.UserDTO;

import java.security.Principal;

public interface UserDtoService extends DtoGenericService<User, UserDTO> {
}
