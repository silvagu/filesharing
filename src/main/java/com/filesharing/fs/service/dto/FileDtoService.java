package com.filesharing.fs.service.dto;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.dto.FileDTO;
import com.filesharing.fs.model.dto.OwnedSharedDTO;
import java.util.Set;

public interface FileDtoService extends DtoGenericService<File, FileDTO> {
  OwnedSharedDTO toDto(Set<File> filesOwned, Set<File> filesShared);
}
