package com.filesharing.fs.service.dto;

import java.util.Set;
import java.util.stream.Collectors;

public interface DtoGenericService<ENTITY, DTO> {

  DTO toDto(ENTITY entity);

  default Set<DTO> toDto(Set<ENTITY> entities) {
    return entities == null ? null : entities.stream().map(this::toDto).collect(Collectors.toSet());
  }

  ENTITY toEntity(DTO dto);
}
