package com.filesharing.fs.service;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;

import java.util.Optional;

public interface UserService {

  void createUser(User user);

  Optional<User> getUserByEmail(String email);

  boolean shareFileWithUser(String ownerEmail, String shareEmail, File file);
}
