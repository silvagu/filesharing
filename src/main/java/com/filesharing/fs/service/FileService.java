package com.filesharing.fs.service;

import com.filesharing.fs.model.File;
import com.filesharing.fs.model.User;
import org.springframework.core.io.Resource;
import java.util.Optional;
import java.util.Set;

public interface FileService {

  Set<File> getFilesOwnedByUserId(Long userId);

  Optional<File> getFileById(Long fileId);

  void saveFile(File file);

  Optional<Resource> getUserFile(User user, Long fileId);
}
