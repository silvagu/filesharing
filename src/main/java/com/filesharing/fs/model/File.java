package com.filesharing.fs.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "files")
public class File {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @EqualsAndHashCode.Include
  private Long id;

  @Column(name = "filename", nullable = false)
  @EqualsAndHashCode.Include
  private String fileName;

  @OneToOne(targetEntity = User.class)
  @JoinColumn(name = "owner_id")
  private User user;

  @ManyToMany(mappedBy = "files")
  private Set<User> sharedUsers = new HashSet<>();
}
