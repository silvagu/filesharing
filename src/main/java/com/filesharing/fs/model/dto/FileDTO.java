package com.filesharing.fs.model.dto;

import com.filesharing.fs.model.File;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDTO {
  private String id;
  private String fileName;

  public FileDTO(File file) {
    id = file.getId().toString();
    fileName = file.getFileName();
  }
}
