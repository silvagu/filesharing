package com.filesharing.fs.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OwnedSharedDTO {

  private Set<FileDTO> owned;
  private Set<FileDTO> shared;
}
