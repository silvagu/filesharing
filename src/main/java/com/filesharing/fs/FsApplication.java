package com.filesharing.fs;

import com.filesharing.fs.config.DataDBInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@EnableTransactionManagement
public class FsApplication {

  public static void main(String[] args) {
    SpringApplication.run(FsApplication.class, args);
  }

  @Bean(initMethod = "init")
  @PostConstruct
  public DataDBInitializer initData() {
    return new DataDBInitializer();
  }
}
