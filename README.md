# File Sharing

### About
> Service for file sharing between users..

### Documentation

### Build Setup &nbsp; :construction:

``` 
mvn package
```

### Getting started &nbsp;:running:

``` 
java -jar FileSharing.jar
```

Or with a specific upload directory
``` 
java -jar FileSharing.jar --upload.directory=/path/to/another/dir
```

### Api endpoints examples (using curl)

Register an user:
``` 
curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"email": "silva@gustavo.com", "password": "1234"}' \
    http://localhost:8080/register
```

Get files owned by authenticated user and files that are shared with him.
``` 
curl --header "Content-Type: application/json" \
    --user silva@gustavo.com:1234 \
    http://localhost:8080/api/file
```

Download a file, specified by the given id. You will be able to download only the files those are awned by you and shared with you.
```
curl http://localhost:8080/api/file/1 \                          
    --output download.txt \
    --user admin@me.com:admin
```

Upload a file. It will return file ID that can be used with GET endpoint to download a file.
```
curl --request POST \
    --form 'file=@/Users/gustavo/download.txt' \
    --user silva@gustavo.com:1234 \
    http://localhost:8080/api/file
```

Share file with another user.
```
curl --header "Content-Type: application/json" \
    --request POST \
    --data '{"email": "silva@gustavo.com", "fileId": "1"}' \
    --user admin@me.com:admin \
    http://localhost:8080/api/share
```

### Built With

* [SprintBoot](https://spring.io/projects/spring-boot) - Java-based framework used to create a micro Service..
* [Hibernate](https://hibernate.org/) - Object/Relational Mapping (ORM) framework.
* [Lombok](https://projectlombok.org/) - java library that automatically plugs into your editor and build tools.
* [H2](https://www.h2database.com/html/main.html) - Java SQL database.
* [Maven](https://maven.apache.org/) - Project management and comprehension tool..

## Authors

* **Gustavo Silva** - *Initial work* 

## License

This project is licensed under the MIT License - see the LICENSE.md file for details